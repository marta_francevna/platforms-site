<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Complaint */

$this->title = 'Жалоба';
$this->params['breadcrumbs'][] = ['label' => 'Жалобы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->nickname;
?>
<div class="complaint-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Имя',
                'attribute' => 'nickname',

            ],
            'email:email',
            [
                'label' => 'Сообщение',
                'attribute' => 'message',

            ],
            [
                'label' => 'Отправлено',
                'attribute' => 'created_at',

            ],
        ],
    ]) ?>

</div>
