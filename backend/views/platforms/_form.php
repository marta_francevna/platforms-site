<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Platforms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="platforms-form">

    <?php $form = \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название') ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
        'clientOptions' => [
            'filebrowserUploadUrl' => '/admin/upload',
        ]
    ]) ?>

    <?= $form->field($model, 'working_height')->textInput(['maxlength' => true])->label('Рабочая высота') ?>

    <?= $form->field($model, 'working_capacity')->textInput()->label('Грузоподъемность') ?>

    <?= $form->field($model, 'arrow')->textInput(['maxlength' => true])->label('Тип стрелы') ?>

    <?= $form->field($model, 'seats')->textInput()->label('Кол-во мест') ?>

    <?= $form->field($model, 'type')
        ->dropDownList(
            [
                1 => 'Автовышка',
                2 => 'Погрузчик',
                3 => 'Подъёмник'
            ],
            ['prompt' => '']
        ); ?>


    <?php if($model->image) {?>
    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
        'pluginOptions' => [
            'initialPreview'=>[
                Html::img( Yii::$app->urlManagerFrontEnd->baseUrl. '/img/' . $model->image)
            ],
            'overwriteInitial'=>true
        ],
        'options' => ['accept' => 'image/*'],
    ]); ?>
    <?php }else{ ?>
	<?= $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
		'options' => ['accept' => 'image/*'],
	]); ?>

    <?php } ?>

    <?= $form->field($model, 'eparture')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rise_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transport_speed')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'angle_of_rotation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operating_weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transport_height')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transport_length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transport_width')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chassis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'power')->textInput(['maxlength' => true]) ?>

    <?php if($model->scheme) {?>
        <?= $form->field($model, 'scheme')->widget(\kartik\file\FileInput::classname(), [
            'pluginOptions' => [
                'initialPreview'=>[
                    Html::img( Yii::$app->urlManagerFrontEnd->baseUrl. '/img/' . $model->scheme)
                ],
                'overwriteInitial'=>true
            ],
            'options' => ['accept' => 'image/*'],
        ]); ?>
    <?php }else{ ?>
        <?= $form->field($model, 'scheme')->widget(\kartik\file\FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
        ]); ?>

    <?php } ?>


    <?= $form->field($model, 'amount')->textInput(['maxlength' => true])->label('Стоимость') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
