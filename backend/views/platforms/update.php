<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Platforms */

$this->title = 'Изменить оборудование: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="platforms-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
