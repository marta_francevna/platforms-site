<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Platforms */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platforms-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php $items = [
        [
            'label' => 'Название',
            'attribute' => 'name',
        ],
            [
                'label' => 'Тип',
                'attribute' => 'type',
                'value' => function ($model) {
                    if ($model->type == 1) {
                        return 'Автовышка';
                    } elseif ($model->type == 2){
                        return 'Погрузчик';
                    }
                    else {
                        return 'Подъемник';
                    }
                }
            ],
            [
                'label' => 'Рабочая высота',
                'attribute' => 'working_height',
            ],
            [
                'label' => 'Грузоподъемность',
                'attribute' => 'working_capacity',
            ],
            [
                'label' => 'Тип стрелы',
                'attribute' => 'arrow',
            ],
            [
                'label' => 'Кол-во мест',
                'attribute' => 'seats',
            ],
        [
            'label' => 'Тип стрелы',
            'attribute' => 'arrow',
        ],
        [
            'label' => 'Вылет',
            'attribute' => 'eparture',
        ],
        [
            'label' => 'Время подъёма',
            'attribute' => 'rise_time',
        ],
        [
            'label' => 'Транспортная скорость',
            'attribute' => 'transport_speed',
        ],
        [
            'label' => 'Угол поворота',
            'attribute' => 'angle_of_rotation',
        ],
        [
            'label' => 'Эксплуатационная масса',
            'attribute' => 'operating_weight',
        ],
        [
            'label' => 'Транспортная высота',
            'attribute' => 'transport_height',
        ],
        [
            'label' => 'Транспортная длина',
            'attribute' => 'transport_length',
        ],
        [
            'label' => 'Транспортная ширина',
            'attribute' => 'transport_width',
        ],
        [
            'label' => 'Шасси',
            'attribute' => 'chassis',
        ],
        [
            'label' => 'Мощность',
            'attribute' => 'power',
        ],

        [
            'label' => 'Изображение',
            'attribute' => 'image',
            'value'=>  Yii::$app->urlManagerFrontEnd->baseUrl. '/img/' . $model->image,
            'format' => ['image',['width'=>'100','height'=>'100']],
        ],
        [
            'label' => 'Схема',
            'attribute' => 'scheme',
            'value'=>  Yii::$app->urlManagerFrontEnd->baseUrl. '/img/' . $model->scheme,
            'format' => ['image',['width'=>'100','height'=>'100']],
        ],
            [
                'label' => 'Стоимость',
                'attribute' => 'amount',
            ],

        ];
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $items
    ]) ?>

</div>
