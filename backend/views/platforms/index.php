<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PlatformsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оборудование';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platforms-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Новое оборудование', ['create'], ['class' => 'btn btn-success']) ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Тип',
                'attribute' => 'type',
                'value' => function ($model) {
                    if ($model->type == 1) {
                        return 'Автовышка';
                    } elseif ($model->type == 2){
                        return 'Погрузчик';
                    }
                    else {
                        return 'Подъемник';
                    }
                }
            ],
            [
                'label' => 'Рабочая высота',
                'attribute' => 'working_height',
            ],
            [
                'label' => 'Грузоподъемность',
                'attribute' => 'working_capacity',
            ],
            [
                'label' => 'Тип стрелы',
                'attribute' => 'arrow',
            ],
            [
                'label' => 'Кол-во мест',
                'attribute' => 'seats',
            ],

            [
                'label' => 'Стоимость',
                'attribute' => 'amount',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
