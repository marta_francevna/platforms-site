<?php
use yii\bootstrap\Nav;

?>
<aside class="left-side sidebar-offcanvas">

    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest) : ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>Привет, <?= @Yii::$app->user->identity->username ?></p>
                    <a href="<?= $directoryAsset ?>/#">
                        <i class="fa fa-circle text-success"></i> Online
                    </a>
                </div>
            </div>
        <?php endif ?>

       <?php
        $menuItems = [];
        if (!Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => '<span class="fa fa-shopping-cart"></span> Заказы', 'url' => ['/order'],  'active' => in_array(\Yii::$app->controller->id, ['order'])];
            $menuItems[] = ['label' => '<span class="fa fa-envelope-open-o"></span> Обратная связь', 'url' => ['/feedback'],  'active' => in_array(\Yii::$app->controller->id, ['feedback'])];
            $menuItems[] = ['label' => '<span class="fa fa-user-circle-o"></span> Жалобы', 'url' => ['/complaint'],  'active' => in_array(\Yii::$app->controller->id, ['complaint'])];
            $menuItems[] = ['label' => '<span class="fa fa-question-circle"></span> Вопросы', 'url' => ['/issues'],  'active' => in_array(\Yii::$app->controller->id, ['issues'])];
            $menuItems[] = ['label' => '<span class="fa fa-truck"></span> Оборудование', 'url' => ['/platforms'], 'active' => in_array(\Yii::$app->controller->id, ['platforms'])];
            $menuItems[] = ['label' => '<span class="fa fa-newspaper-o"></span> Статьи', 'url' => ['/articles'], 'active' => in_array(\Yii::$app->controller->id, ['article'])];
            $menuItems[] = ['label' => '<span class="fa fa-file"></span> Документы', 'url' => ['/documents'], 'active' => in_array(\Yii::$app->controller->id, ['documents'])];
            $menuItems[] = ['label' => '<span class="fa fa-wrench"></span> Настройки', 'url' => ['/settings'], 'active' => in_array(\Yii::$app->controller->id, ['settings'])];
        } ?>
        <?=
        Nav::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menuItems
            ]
        );
        ?>



    </section>

</aside>
