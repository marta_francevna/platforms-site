<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Оборудование',
                'attribute' => 'platform.name',
            ],
            [
                'label' => 'Имя',
                'attribute' => 'nickname',

            ],
            [
                'label' => 'Телефон',
                'attribute' => 'phone',

            ],
            'email:email',
            [
                'label' => 'Сообщение',
                'attribute' => 'message',

            ],
            [
                'label' => 'Отправлено',
                'attribute' => 'created_at',

                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'dateFormat' => 'php:Y-m-d',
                    'options' => [
                        'class' => 'form-control',
                    ],
                ]),
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'template' => '{view}{delete}',

            ]
        ],
    ]); ?>
</div>
