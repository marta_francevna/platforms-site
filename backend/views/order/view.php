<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = 'Заказ';
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Просмотор';
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php $items = [
	    [
		    'label' => 'Имя',
		    'attribute' => 'nickname',
	    ],
	    [
		    'label' => 'Телефон',
		    'attribute' => 'phone',
	    ],
	    'email:email',
	    [
		    'label' => 'Сообщение',
		    'attribute' => 'message',
	    ],
	    [
		    'label' => 'Отправлено',
		    'attribute' => 'created_at',

	    ],
    ];
    if ($model->platform) {
        $items[] = [
            'label' => 'Оборудование',
            'attribute' => 'platform.name',

        ];
    }
    if ($model->image) {
	    $items[] = [
		    'label' => 'Изображение',
		    'attribute' => 'image',
		    'value'=>  Yii::$app->urlManagerFrontEnd->baseUrl. '/uploads/' . $model->image,
		    'format' => ['image',['width'=>'100','height'=>'100']],
	    ];

    }

    ?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $items
    ]) ?>

</div>
