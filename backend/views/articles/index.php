<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ArticlesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новая статья', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Заголовок',
                'attribute' => 'title',

            ],
            [
                'label' => 'Описание',
                'attribute' => 'description',

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='/article/'.$model->slug;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url ='/articles/update?id='.$model->id;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='/articles/delete?id='.$model->id;
                        return $url;
                    }
                }
            ],
        ],
    ]); ?>
</div>
