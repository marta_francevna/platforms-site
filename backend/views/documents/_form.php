<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Documents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="documents-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if($model->file) {?>
    <?= $form->field($model, 'file')->widget(\kartik\file\FileInput::classname(), [
        'pluginOptions' => [
            'initialPreview' => [
                Yii::$app->urlManagerFrontEnd->baseUrl . '/doc/' . $model->file
            ],
            'browseIcon' => '<i class="fa fa-file-word-o"></i> ' ,

            'previewFileIcon' => '<i class="fa fa-file"></i>',
            'allowedPreviewTypes' => null,
            'previewFileIconSettings' => [
                'docx' => '<i class="fa fa-file"></i>',
                'doc' => '<i class="fa fa-file"></i>',
            ],
        ],


        'options' => ['accept' => '.doc, .docx, .pdf'],
    ]); ?>
    <?php }else{ ?>
        <?= $form->field($model, 'file')->widget(\kartik\file\FileInput::classname(), [
            'options' =>['accept' => '.doc, .docx, .pdf'],
        ]); ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
