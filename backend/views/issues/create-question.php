<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Issues */

$this->title = 'Новый вопрос';
$this->params['breadcrumbs'][] = ['label' => 'Вопросы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issues-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="issues-form">

        <?php $form = \yii\widgets\ActiveForm::begin(); ?>

        <?= $form->field($model, 'nickname')->textInput(['maxlength' => true])->label('Имя') ?>
        <?= $form->field($model, 'email')->input('email')->label('Email') ?>
        <?= $form->field($model, 'message')->textarea(['maxlength' => true])->label('Вопрос') ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>

</div>
