<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\IssuesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issues-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Создать вопрос', ['create-question'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Имя',
                'attribute' => 'nickname',
            ],
            'email:email',
            [
                'label' => 'Вопрос',
                'attribute' => 'message',
            ],
            [
                'label' => 'Отправлено',
                'attribute' => 'created_at',

                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'dateFormat' => 'php:Y-m-d',
                    'options' => [
                        'class' => 'form-control',
                    ],
                ]),
            ],
            [
                'label' => 'Ответ',
                'attribute' => 'question.message',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'template' => '{view}{delete}',
                'buttons'=>[
                    'view' => function ($url, $model) {
                         if (!$model->question){
                             return Html::a('<span class="glyphicon glyphicon-plus"></span>','/issues/create?id='.$model->id , [
                                 'title' => Yii::t('yii', 'Ответить'),
                             ]);
                         }else {
                             return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/issues/update?id='.$model->id, [
                                 'title' => Yii::t('yii', 'Изменить'),
                             ]);
                         }
                    }
                    ]

            ]
        ],
    ]); ?>
</div>
