<?php

namespace backend\controllers;

use Yii;
use common\models\Issues;
use common\models\IssuesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IssuesController implements the CRUD actions for Issues model.
 */
class IssuesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Issues models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IssuesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionView($id)
    {
        $model = new Issues();

        $model->question_id = $id;
        $model->nickname = 'Администратор.';
        $model->email = 'admin@gmail.com';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/issues');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionCreate($id)
    {
        $model = new Issues();

        $model->question_id = $id;
        $model->nickname = 'Администратор.';
        $model->email = 'admin@gmail.com';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/issues');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateQuestion(){

        $model = new Issues();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/issues']);
        }

        return $this->render('create-question', [
            'model' => $model,
        ]);

    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = Issues::find()->where(['question_id'=>$id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/issues');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $model = Issues::find()->where(['question_id'=>$id])->one();
        if ($model){
            $model ->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Issues model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Issues the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Issues::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
