<?php

namespace backend\controllers;

use common\forms\LoginForm;
use Faker\Provider\File;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class AdminController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        if (!Yii::$app->user->isGuest) {
            $this->redirect('/order');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect('/order');
        } else {
            $model->password = '';

            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        $this->redirect('/');
    }

    public function actionUpload($CKEditorFuncNum)
    {
        $file = UploadedFile::getInstanceByName('upload');
        if ($file) {
            if($file->saveAs(dirname(dirname(__DIR__)) . '/frontend/web/img/' . $file->baseName . '.' . $file->extension, false)){
                return '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $CKEditorFuncNum . '", "' . Yii::$app->urlManagerFrontEnd->baseUrl. '/uploads/' .$file . '", "");</script>';
            } else return "Возникла ошибка при загрузке файла\n";
        } else return "Файл не загружен\n";
    }


}
