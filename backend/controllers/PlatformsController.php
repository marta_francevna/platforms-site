<?php

namespace backend\controllers;

use Yii;
use common\models\Platforms;
use common\models\IssuesSearchPlatforms;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PlatformsController implements the CRUD actions for Platforms model.
 */
class PlatformsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Platforms models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IssuesSearchPlatforms();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Platforms model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Platforms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Platforms();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->image = UploadedFile::getInstance($model, 'image')) {
                $model->image->saveAs(dirname(dirname(__DIR__)) . '/frontend/web/img/' . $model->image->baseName . '.' . $model->image->extension, false);
            }
            if ($model->scheme = UploadedFile::getInstance($model, 'scheme')) {
                $model->image->saveAs(dirname(dirname(__DIR__)) . '/frontend/web/img/' . $model->scheme->baseName . '.' . $model->scheme->extension, false);
            }
            if ($model->save(false)) {
                return $this->redirect(['/platforms']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Platforms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;
        $oldSchemeImage = $model->scheme;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $image = UploadedFile::getInstance($model, 'image');
            $scheme = UploadedFile::getInstance($model, 'scheme');
            if ($image) {
                $model->image = $image;
                $model->image->saveAs(dirname(dirname(__DIR__)) . '/frontend/web/img/' . $model->image->baseName . '.' . $model->image->extension, false);
            }else{
                $model->image = $oldImage;
            }
            if ($scheme) {
                $model->scheme = $scheme;
                $model->scheme->saveAs(dirname(dirname(__DIR__)) . '/frontend/web/img/' . $model->scheme->baseName . '.' . $model->scheme->extension, false);
            }else{
                $model->scheme = $oldSchemeImage;
            }
            if ($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Platforms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Platforms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Platforms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Platforms::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
