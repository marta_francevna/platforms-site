<?php

use yii\widgets\ListView;
\frontend\assets\ArticleAsset::register($this);
/* @var $this yii\web\View */

$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">Статьи</span>
    </nav>
</div>
<div class="container">
    <h3 class="pt-3">Статьи</h3>
    <div class="line mb-2"></div>
</div>
<div class="container-fluid articles">
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_list',
        'summary'=>'',
    ]);
    ?>
</div>