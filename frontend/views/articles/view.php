<?php
use yii\helpers\Html;
\frontend\assets\ArticleAsset::register($this);
/* @var $this yii\web\View
 * @var common\models\Articles $model
*/


$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <a class="breadcrumb-item" href="/articles">Статьи</a>
        <span class="breadcrumb-item active"><?= Html::encode($model->title) ?></span>
    </nav>
</div>
<div class="container">
    <h3 class="pt-3"><?= $model->title ?></h3>
    <div class="line"></div>
    <div class="article">
        <?=$model->content?>
    </div>
</div>
