<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
?>
<a href="/article/<?= $model->slug ?>">
    <div class="article-item">
        <div class="container">
            <div class="title"><?= Html::encode($model->title) ?></div>
            <div class="desc">
                <p><?= HtmlPurifier::process($model->description) ?></p>
            </div>
        </div>
    </div>
</a>