<?php

use yii\helpers\Html;

\frontend\assets\PlatformAsset::register($this);
/* @var $this yii\web\View
 * @var common\models\Platforms $platform
 */

$this->title = $platform->name;
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <a class="breadcrumb-item" href="/site/rental-information/platform">Аренда автовышки</a>
        <span class="breadcrumb-item active"><?= Html::encode($platform->name) ?></span>
    </nav>
</div>
<div class="container platform">
    <div class="row">
        <div class="col-12 col-md-4">
            <img src="/img/<?= $platform->image ?>" alt="Автовышки" class="image">
            <a href="/order/<?= $platform->id ?>" class="btn btn-primary white order-btn">Заказать</a>
        </div>
        <div class="col-12 col-md-8">
            <p class="title"><?= Html::encode($platform->name) ?></p>
            <?php if ($platform->description) { ?>
                <div class="description">
                    <?= $platform->description ?>
                </div>
            <?php } ?>
            <section>
                <p class="table-title">Технические характеристики,
                                                  описание "<?= Html::encode($platform->name) ?>":</p>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <?php if ($platform->working_height) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('working_height') ?></td>
                                <td><?= $platform->working_height ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->working_capacity) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('working_capacity') ?></td>
                                <td><?= $platform->working_capacity ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->arrow) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('arrow') ?></td>
                                <td><?= $platform->arrow ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->seats) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('seats') ?></td>
                                <td><?= $platform->seats ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->eparture) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('eparture') ?></td>
                                <td><?= $platform->eparture ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->rise_time) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('rise_time') ?></td>
                                <td><?= $platform->rise_time ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->transport_speed) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('transport_speed') ?></td>
                                <td><?= $platform->transport_speed ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->angle_of_rotation) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('angle_of_rotation') ?></td>
                                <td><?= $platform->angle_of_rotation ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->operating_weight) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('operating_weight') ?></td>
                                <td><?= $platform->operating_weight ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->transport_height) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('transport_height') ?></td>
                                <td><?= $platform->transport_height ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->transport_length) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('transport_length') ?></td>
                                <td><?= $platform->transport_length ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->transport_width) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('transport_width') ?></td>
                                <td><?= $platform->transport_width ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->chassis) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('chassis') ?></td>
                                <td><?= $platform->chassis ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($platform->power) { ?>
                            <tr>
                                <td><?= $platform->getAttributeLabel('power') ?></td>
                                <td><?= $platform->power ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </section>
            <?php if ($platform->scheme){ ?>
                <a class="image-popup-vertical-fit" href="/img/<?= $platform->scheme ?>" >
                    <img src="/img/<?= $platform->scheme ?>" alt="Схема" class="scheme">
                </a>

            <?php } ?>

        </div>
    </div>
</div>

