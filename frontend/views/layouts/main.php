<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

$mts =  \common\models\Settings::find()->where(['label'=>'Мтс'])->one()->text;
$velcom = \common\models\Settings::find()->where(['label'=>'Velcom'])->one()->text;
$life = \common\models\Settings::find()->where(['label'=>'Life'])->one()->text;
$fax = \common\models\Settings::find()->where(['label'=>'Факс'])->one()->text;
$adress = \common\models\Settings::find()->where(['label'=>'Футер адрес'])->one()->text;

AppAsset::register($this);

$action = Yii::$app->controller->action->id;

$controller = Yii::$app->controller->id;


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody();

?>

<header>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-5 col-lg-7 logo">
                <a href="/">
                    <img src="/img/logo.svg" alt="Логотип автовышки">
                </a>
            </div>
            <div class="col-12 col-sm-4 col-md-3 col-lg-2 time-work">
                <ul class="list-unstyled">
                    <li>Время работы:</li>
                    <li>c 8:00 до 22:00</li>
                    <li>без выходных</li>
                </ul>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 phones">
                <p class="velcom"><a href="tel:<?= $velcom; ?>"><?= $velcom; ?></a></p>
                <p class="mts"><a href="tel:+<?= $mts; ?>"><?= $mts; ?></a></p>
                <p class="life"><a href="tel:<?= $life;?>"><?= $life;?></a></p>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-expand-lg navbar-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav navbar-nav">
            <li class="nav-item <?php echo $action == 'index' && $controller == 'site' ? 'active' : '' ?>">
                <a title="Главная" class="link" href="/">Главная <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item <?php echo $action == 'about' && $controller == 'site' ? 'active' : '' ?>">
                <?= Html::a('О компании', ['site/about'], ['class' => 'link', 'title'=>'О компании']) ?>
            </li>
            <li class="nav-item <?php echo $action == 'documents' && $controller == 'site' ? 'active' : '' ?>">
                <?= Html::a('Документы', ['site/documents'], ['class' => 'link', 'title'=>'Документы']) ?>
            </li>
            <li class="nav-item <?php echo $action == 'index' && $controller == 'articles' ? 'active' : '' ?>">
                <?= Html::a('Статьи', ['/articles'], ['class' => 'link', 'title'=>'Статьи']) ?>
            </li>
            <li class="nav-item <?php echo $action == 'index' && $controller == 'contacts' ? 'active' : '' ?>">
                <?= Html::a('Контакты', ['/contacts'], ['class' => 'link', 'title'=>'Контакты']) ?>
            </li>
        </ul>
    </div>
</nav>
<?= $content ?>

<footer>
    <div class="container footer">
        <div class="row">
            <div class="col-12 col-md-3 col-lg-4">
                <div class="logo">
                    <a href="/"><img src="/img/logo.svg" alt="Логотип автовышки"></a>
                </div>
            </div>
            <div class="col-md-9 col-lg-8 col-12 pl-0 wrapper-footer-nav">
                <ul class="list-unstyled footer-menu">
                    <li class="pl-0"><a href="/">Главная</a></li>
                    <li><a href="/site/about">О компании</a></li>
                    <li><a href="/site/documents">Документы</a></li>
                    <li><a href="/articles">Статьи</a></li>
                    <li><a href="/contacts">Контакты</a></li>
                </ul>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-12 col-sm-9 col-md-10 col-lg-4 pt-1">
                <?= $adress ?>
            </div>
            <div class="col-12 col-md-2 col-sm-3 col-lg-2 px-0">
                <ul class="list-unstyled time-work mb-0">
                    <li>Время работы:</li>
                    <li>c 8:00 до 22:00</li>
                    <li>без выходных</li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-sm-6 col-lg-3 px-0 pt-1 phones-footer">
                <p class="velcom"><a class="phone" href="tel:<?= $velcom?>"><?= $velcom?></a></p>
                <p class="mts"><a class="phone" href="tel:<?= $mts?>"><?= $mts?></a></p>
            </div>
            <div class="col-12 col-md-4 col-sm-6 col-lg-3 px-0  pt-1 phones-footer">
                <p class="life"><a class="phone" href="tel:<?= $life?>"><?= $life?></a></p>
                <p class="fax"><a class="phone" href="#"><?= $fax?><span> (факс)</span></a></p>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
<?php $this->endPage() ?>
