<?php

/* @var $this yii\web\View */

\frontend\assets\QuestionsAsset::register($this);

$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">Вопрос-ответ</span>
    </nav>
</div>
<div class="container">
    <h3 class="pt-3">Вопрос-ответ</h3>
    <?php foreach ($issues as $issue){ ?>
        <?php if (!$issue->question) continue ?>
        <div class="line mb-3"></div>
    <div class="question-answer">
        <div class="question">
            <a href="#" class="spoiler_links">
                <i class="icon-arrow-down icons"></i>
            </a>
            <p class="name"><?php echo $issue->nickname?></p>
            <p class="date"><?php echo date("d.m.Y", strtotime($issue->created_at))?></p>
            <p class="text"><?php echo $issue->message?></p>
        </div>
        <div class="answer">
            <p class="name"><?php echo $issue->question->nickname?></p>
            <p class="text"><?php echo $issue->question->message?></p>
        </div>
    </div>
    <?php }?>
</div>
<div class="container question-form">
    <h3>Задать вопрос</h3>
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
    <?php
    $form = \yii\widgets\ActiveForm::begin([
        'options' => [
            'class' => 'question-fields',
        ]
    ]) ?>
    <div class="row">
        <div class="col-12 col-lg-6 pl-3 inputs">
            <div class="form-group">
                <?= $form->field($model, 'nickname')->label('Имя*'); ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'email')->input('email')->label('E-mail*'); ?>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="form-group message">
                <?= $form->field($model, 'message')->textarea()->label('Текст сообщения*') ?>
            </div>

            <button class="btn btn-primary" type="submit">Отправить</button>
        </div>
        <?php \yii\widgets\ActiveForm::end() ?>

    </div>
</div>