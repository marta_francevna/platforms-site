<?php

/* @var $this yii\web\View */
\frontend\assets\OrderAsset::register($this);

$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <a class="breadcrumb-item" href="/site/rental-information">Автовышки</a>
        <span class="breadcrumb-item active">Заказ</span>
    </nav>
</div>
<div class="container order-form">
    <h3>Заказать автовышку</h3>
    <div class="line"></div>
    <div class="order">
        <p class="mb-4">Для заказа автовышки, просим Вас заполнить заявку. Наш оператор свяжется с Вами, ответит на вопросы и поможет
            заказать автовышки, наиболее соответствующие Вашим запросам.</p>
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>
        <?php
       $form = \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <div class="row">
                <div class="col-12 col-lg-6 inputs">
                    <div class="form-group">
                        <div class="form-group">
                            <?= $form->field($model, 'nickname')->label('Имя*'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php \yii\widgets\MaskedInput::widget([
                            'model' => $model,
                            'attribute' => 'phone',
                            'name' => 'phone',
                            'mask' => '+375(99) 999-99-99'
                        ]);
                        ?>
                        <?= $form->field($model, 'phone')->label('Телефон*'); ?>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <?= $form->field($model, 'email')->input('email')->label('E-mail*'); ?>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="file-upload">
                            <label>
                                <?= $form->field($model, 'image')->fileInput(['accept'=>".png, .jpg, .jpeg"])->label('');?>
                                <span>Приложить файл</span>
                            </label>
                        </div>
                        <input type="text" id="filename" class="filename" value = 'Файл не выбран' disabled>
                    </div>
                </div>
                <div class="col-12 col-lg-6 message">
                    <div class="form-group">
                        <div class="form-group">
                            <?= $form->field($model, 'message')->textarea()->label('Текст сообщения*') ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'platform_id')->hiddenInput()->label('') ?>

                    <button class="btn btn-primary" type="submit">Отправить</button>
                </div>
            </div>

        <?php \yii\widgets\ActiveForm::end() ?>
    </div>
</div>
