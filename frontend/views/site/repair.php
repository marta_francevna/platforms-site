<?php

\frontend\assets\RepairAsset::register($this);

/* @var $this yii\web\View */
$settings = \common\models\Settings::find()->one();
$phones = json_decode($settings->numbers, true);
$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">Ремонт автовышки</span>
    </nav>
</div>
<div class="container-fluid key">
    <div class="container">
        <h3 class="pt-3">Ремонт автовышки: гарантированное качество работ</h3>
        <div class="line"></div>
        <div class="repairs">
            <p>В нашей компании вы можете заказать следующие услуги:</p>
            <ul>
                <li>Диагностику автоподъемников.</li>
                <li>Ремонтные работы в отношении гидрооборудования.</li>
                <li>Ремонт электрооборудования.</li>
                <li>Плановое техническое обслуживание оборудования (ТО).</li>
                <li>Шиномонтаж.</li>
            </ul>
            <p>Сделайте выгодный заказ услуги ремонта автовышки или получите <br> ответы на все ваши вопросы - по тел.
                <a class="tel-bold" href="tel:<?= $phones['life']?>"><?= $phones['life']?>.</a></p>
            <p class="repair-bold">Мы за результативный сервис для вашей техники и внимательное отношение к вам как к клиенту!</p>
        </div>
    </div>
</div>
