<?php

/* @var $this yii\web\View */
$mts =  \common\models\Settings::find()->where(['label'=>'Мтс'])->one()->text;
$velcom = \common\models\Settings::find()->where(['label'=>'Velcom'])->one()->text;
$life = \common\models\Settings::find()->where(['label'=>'Life'])->one()->text;
$fax = \common\models\Settings::find()->where(['label'=>'Факс'])->one()->text;
$email = \common\models\Settings::find()->where(['label'=>'Email'])->one()->text;
\frontend\assets\AboutAsset::register($this);
$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">О компании</span>
    </nav>
</div>
<div class="container">
    <h3 class="pt-3">О компании</h3>
    <div class="line"></div>
    <div class="row">
        <div class="col-md-8 pr-5">
            <div class="company-text " >
                <?= \common\models\Settings::find()->where(['label'=>'О компании'])->one()->text; ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="company-info">
                <p class="text-bold">Телефоны</p>
                <p class="velcom"><a href="tel:<?= $velcom?>"><?= $velcom?></a></p>
                <p class="mts"><a href="tel:<?= $mts ?>"><?= $mts?></a></p>
                <p class="life"><a href="tel:<?= $life?>"><?= $life?></a></p>
                <p class="fax"><a href="#"><?= $fax?><span> (факс)</span></a></p>
                <p class="text-bold pt-2">E-mail</p>
                <p class="email"><a href="mailto:<?= $email?>"><?= $email?></a></p>
            </div>
        </div>
    </div>
</div>
