<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

\frontend\assets\IndexAsset::register($this);

$this->title = 'Аренда спецтехники в Минске';
?>
<section>
    <div class="container-fluid lease-background">
        <div class="container main-lease">
            <h3>Аренда спецтехники в Минске
                <br> и по всей территории РБ: техника от 12 до 40 метров.</h3>
            <ul class="list-unstyled">
                <li>Собственный автопарк 8 машин.</li>
                <li>Возможна работа в праздничные и выходные дни.</li>
                <li>Возможна работа более 8 часов в сутки.</li>
                <li>При заказе от 2 ед. скидки до 20 %.</li>
            </ul>
            <?= Html::a('Арендовать', ['/order'], ['class' => 'btn btn-primary']) ?>
            <img src="/img/main1.png" alt="Автовышки" class="lease-image">
        </div>
    </div>
</section>
<section>
    <div class="container select-technology mb-5">
        <h3 class="center">Выберите специальную технику</h3>
        <div class="machine mb-4">
            <div class="min-line"></div>
            <img src="/img/lever-machine.svg" alt="Автовышки">
            <div class="min-line"></div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-auto ">
		        <?= Html::a('Аренда автовышки', ['site/rental-information/platform'], ['class' => 'btn btn-primary lease-btn']) ?>
            </div>
            <div class="col-md-12 col-lg-auto">
                <?= Html::a('Аренда телескопического<br> погрузчика', ['site/rental-information/loader'], ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="col-md-12 col-lg-auto">
                <?= Html::a('Аренда самоходных<br> подъемников', ['site/rental-information/lift'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="row mt-1 pb-3">
            <div class="col-xl-auto col-lg-4 pt-2 mr-2">
                <img src="/img/lease-1.png" alt="Автовышки">
            </div>
            <div class="col-xl-auto col-lg-4 pt-4 big-platform  pl-0" >
                <img class="pt-1" src="/img/platform5.png" alt="Автовышки" width="300">
            </div>
            <div class="col-xl-auto col-lg-4 px-0">
                <img class="pt-2" src="/img/lease3.png" alt="Автовышки">
            </div>
        </div>

    </div>
</section>
<section>
    <div class="container-fluid main-desc-background">
        <div class="container main-desc">
            <div class="row pt-5">
                <div class="col-md-12 col-lg-8">
                    <h2 class="pb-3">О компании</h2>
                    <?= \common\models\Settings::find()->where(['label'=>'Главная - о компании'])->one()->text; ?>
                </div>
                <div class="col-md-12 col-lg-auto desc-images">
                    <div class="desc-image">
                        <img src="/img/Group16.svg" alt="Автовышки">
                        <div class="image-text units">
                            <p class="white">Скидка
                                при заказе</p>
                            <p class="silver">от <span>2</span> единиц</p>
                        </div>
                    </div>
                    <div class="desc-image">
                        <img src="/img/Group18.svg" alt="Автовышки">
                        <div class="image-text technique">
                            <p class="white">Скидка
                                при заказе
                                техники </p>
                            <p class="silver">от <span>5</span> смен</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tangle footer">
            <img src="/img/tangle.svg" alt="Автовышки">
        </div>
    </div>
</section>

<section>
    <div class="container  main-links">
        <div class="row">
            <div class="col item">
                <img src="/img/book.svg" alt="Автовышки">
                <?= Html::a('Книга замечаний и<br> предложений', ['/complaint-book']) ?>
            </div>
            <div class="col item">
                <img src="/img/question.svg" alt="Автовышки">
                <?= Html::a('Вопрос-ответ', ['/issues']) ?>
            </div>
            <div class="col item">
                <img src="/img/tools.svg" alt="Автовышки">
                <?= Html::a('Ремонт автовышек', ['site/repair']) ?>
            </div>
        </div>
    </div>
</section>
<!--<section>-->
<!--    <div class="container discounts">-->
<!--        <img src="/img/discounts.svg" alt="Автовышки">-->
<!--    </div>-->
<!--</section>-->

<section>
    <div class="container-fluid discounts-background">
        <div class="container discounts">
            <div class="row pt-2">
                <div class="col-auto">
                    <p class="platform-title">Автовышки</p>
                    <p class="percentages-title">Скидка</p>
                </div>
                <div class="col-auto pl-0">
                    <p class="percentages">5%</p>
                </div>
                <div class="col pt-3 pl-3">
                    <p class="platform">при заказе</p>
                    <p class="blue">от 2 машин</p>
                </div>
                <div class="col pt-3">
                    <p class="platform">на срок</p>
                    <p class="blue">более 5 смен</p>
                </div>
            </div>
        </div>
    </div>
</section>