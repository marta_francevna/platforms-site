<?php

/* @var $this yii\web\View */

\frontend\assets\DocumentsAsset::register($this);

$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">Документы</span>
    </nav>
</div>
<div class="container">
    <h3 class="pt-3">Документы</h3>
    <div class="line"></div>
    <div class="doc-text">
        <?php foreach ($documents as $document) {?>
            <p> <a href="/doc/<?= $document->file ?> "><?= $document->name ?> (<?= $document->size ?>)</a>
        <a href="/doc/<?= $document->file ?> "> <img src="/img/download.svg" alt="Заявка на аренду автоподъемника"></a></p>
        <?php } ?>
    </div>
</div>
