<?php

/* @var $this yii\web\View */

\frontend\assets\LeaseAsset::register($this);

$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">Аренда автовышки</span>
    </nav>
</div>

<div class="container">
    <ul class="nav nav-tabs lease" role="tablist">
        <li class="nav-item">
            <a class="nav-link <?= $tab == 'platform' ? 'active':'' ?>" data-toggle="tab" href="#platform" role="tab">Аренда автовышки</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?= $tab == 'loader' ? 'active':'' ?>" data-toggle="tab" href="#loader" role="tab">Аренда телескопического погрузчика</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?= $tab == 'lift' ? 'active':'' ?>" data-toggle="tab" href="#lift" role="tab">Аренда самоходных подъемников</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane <?= $tab == 'platform' ? 'active':'' ?>" id="platform" role="tabpanel">
            <section>
                <p class="center mt-4">С нами высота не препятствие, а условие безопасного выполнения высотных
                    работ.</p>
                <?php foreach ($platforms as $platform) {?>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 px-0 pt-1">
                        <img src="/img/<?php echo $platform->image?>" alt="Автовышки">
                    </div>
                    <div class="col-12 col-md-6 col-lg-5 px-0">
                        <div class="info">
                            <div class="title"><?php echo $platform->name?></div>
                            <ul class="list-unstyled">
                                <li>Тип стрелы: <?php echo $platform->arrow?></li>
                                <li>Макс.рабочая высота: <?php echo $platform->working_height?> м.</li>
                                <li>Макс. грузоподъемность люльки: <?php echo $platform->working_capacity?> кг.</li>
                                <li>Количество мест: <?php echo $platform->seats?></li>
                            </ul>
<!--                            <a href="/">Смотреть подробней</a>-->
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 px-0">
                        <a href="/platform/<?php echo $platform->id ?>" class="btn btn-primary white" >Подробнее</a>
                    </div>
                </div>
                <?php } ?>

            </section>
            <section>
                <p class="center title-bold py-2">Таблица раcценок на автовышки:</p>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="pb-4">Техника</th>
                            <th>Высота<br> автовышки, м.</th>
                            <th>Цена за смену<br> (8 часов)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $flag = 0 ?>
                        <?php foreach ($platforms as $platform) { $flag++?>
                        <tr>
                            <?php if($flag == 1){ ?>
                            <td rowspan="<?php echo count($platforms) ?>" class="px-0 platform">
                                <img src="/img/platform5.png" alt="Автовышки">
                            </td>
                            <?php } ?>
                            <td><?php echo $platform->working_height ?></td>
                            <td><?php echo $platform->amount ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        <div class="tab-pane <?= $tab == 'loader' ? 'active':'' ?>" id="loader" role="tabpanel">
            <section>
                <p class="pt-4">Аренда телескопического погрузчика MANITOU (Маниту) на привлекательных условиях. Высота
                    подъема от 12 до
                    21 метров. Грузоподъемность до 5000 кг. Все машины могут быть оборудованы вилами, ковшом и
                    поворотной
                    люлькой.</p>
                <div class="row mb-3">
                    <?php $flag = 0 ?>
                    <?php foreach ($loaders as $loader) { $flag++?>
                    <div class="col-12 col-md-6 col-lg-3 item">
                        <img src="/img/<?php echo $loader->image ?>" alt="Автовышки"
                             <?php if($flag == 4) echo 'height="304"' ?>
                        >
                        <p class="description">Высота подъема <?php echo $loader->working_height ?> метров. Грузоподъемность <?php echo $loader->working_capacity ?> кг.</p>
                    </div>
                    <?php } ?>
                </div>
            </section>
            <section>
                <div class="article pb-4">
                    <h3 class="pt-4 pb-2">Телескопический погрузчик в аренду - в компании ЯнТранс</h3>
                    <p>Современный телескопический погрузчик - уникальная универсальная строительная спецтехника с
                        высокими
                        возможностями. Техника для использования, к примеру, в гражданском, дорожном строительстве, при
                        возведении зданий. Погрузчик может работать как внутри, так и снаружи помещений.</p>

                    <p class="mb-2 pt-2">Главные сильные стороны телескопического погрузчика:</p>
                    <ul>
                        <li>Наличие выдвижной стрелы, в том числе, поворотного варианта.</li>
                        <li>Плавность при повороте, небольшой радиус разворота техники.</li>
                        <li>Высокий уровень проходимости.</li>
                        <li>Широкий спектр дополнительного оборудования, с возможностью быстро сменить его.</li>
                        <li>Наличие системы стабилизации груза, повышающей безопасность работы техники.</li>

                    </ul>
                    <p>Наша компания предлагает клиентам услуги аренды погрузчиков Маниту. Спецтехника Маниту создается
                        во Франции. Заказчики в Беларуси ценят высокие рабочие способности этих погрузчиков за
                        возможность использования даже в сложных условиях и при значительной нагрузке.</p>

                    <h3 class="py-2">Аренда телескопического погрузчика</h3>
                    <p>Сделав выбор в пользу арендных услуг Маниту, клиент получает технику, с которой можно стабильно и
                        качественно выполнять высотные работы, включая мероприятия на участках с неровной или
                        обледеневшей поверхностью.</p>

                    <p class="title-bold pt-2">Арендовать телескопический погрузчик в нашей компании - преимущество для
                        заказчика:</p>
                    <ul>
                        <li>Мы предоставляем спецтехнику с любым необходимым навесным оборудованием.</li>
                        <li>Уместным дополнением к умениям техники являются услуги наших опытных операторов.</li>
                        <li>Вся спецтехника сдается в аренду в полностью исправном состоянии.</li>
                    </ul>

                    <p>Сделайте выгодный заказ телескопического погрузчика или получите ответы на все возникшие вопросы
                        по <br>телефонам: +375 (44) 544 55 55, +375 (29) 544 55 55, +375 (25) 544 55 55</p>
                </div>
            </section>
            <section>
                <h3 class="center">Таблица расценок на телескопические погрузчики:</h3>
                <p class="center pb-4">В случае длительных заявок возможна договорная цена и скидки</p>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="pb-4">Техника</th>
                            <th>Высота<br> автовышки, м.</th>
                            <th>Цена за смену<br> (8 часов)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $flag = 0 ?>
                        <?php foreach ($loaders as $loader) { $flag++?>
                            <tr>
                                <?php if($flag == 1){ ?>
                                    <td rowspan="<?php echo count($loaders) ?>" class="pt-4">
                                        <img src="/img/loader-2.png" alt="Автовышки">
                                    </td>
                                <?php } ?>
                                <td><?php echo $loader->working_height ?></td>
                                <td><?php echo $loader->amount ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        <div class="tab-pane <?= $tab == 'lift' ? 'active':'' ?>" id="lift" role="tabpanel">
            <section>
                <div class="row lift-description pt-3">
                    <div class="col-md-12 col-lg-auto">
                        <img src="/img/lift2.jpg" width="492" alt="Автовышки">
                    </div>
                    <div class="col-auto col-sm-7 col-md-6 col-lg-auto desc">
                        <ul class="list-unstyled">
                            <li>Рабочая высота, м.</li>
                            <li>Горизонтальный вылет, м.</li>
                            <li>Размеры платформы, м</li>
                            <li>Вес, кг</li>
                            <li>Тип питания</li>
                            <li>Длина, м</li>
                            <li>Ширина, м</li>
                            <li>Высота подъёма пола платформы, м</li>
                            <li>Внутренний радиус поворота, м</li>
                            <li>Внешний радиус поворота, м</li>
                            <li>Преодолеваемый подъём, %</li>
                            <li>Скорость движения, км/ч</li>
                            <li>Дорожный просвет, см</li>
                            <li>Шины Гусеницы</li>
                            <li>Вращение рабочей платформы</li>
                            <li>Вращение основания подъёмника</li>
                            <li>Высота подъёма пола платформы, м</li>
                            <li>Высота в сложенном состоянии</li>
                        </ul>
                    </div>
                    <div class="col-auto col-sm-4 col-md-6 col-lg-2">
                        <ul class="list-unstyled">
                            <li>16.3</li>
                            <li>9.9</li>
                            <li>1.83 x 0.76</li>
                            <li>7000</li>
                            <li>Дизель</li>
                            <li>6.53</li>
                            <li>2.08</li>
                            <li>16.0</li>
                            <li>1.73</li>
                            <li>4.27</li>
                            <li>10</li>
                            <li>7.2</li>
                            <li>36</li>
                            <li>Внедорожные</li>
                            <li>180</li>
                            <li>360</li>
                            <li>16.0</li>
                            <li>2.25</li>
                        </ul>

                    </div>
                </div>
            </section>
            <section>
                <div class="article pb-4">
                    <h3 class="pt-1 pb-2">Арендовать самоходный подъемник в нашей компании – рационально по нескольким
                        причинам:
                    </h3>

                    <ul>
                        <li>Объемный выбор подъемников и помощь в подборе наилучшего решения.</li>
                        <li>Вся техника сдается в аренду в исправном техническом состоянии.</li>
                        <li>Взять спецтехнику в аренду многократно экономичней, чем приобрести ее в пользование.</li>
                        <li>Современные подъемники в аренду</li>
                        <li>Самоходный подъемник представляет собой спецтехнику, снабженную металлической платформой, и
                            способную работать как внутри помещений, так и на открытых площадях. Эта техника позволяет
                            осуществить подъем человека, груза на заданную высоту.
                        </li>
                    </ul>
                    <h3>Подъемники самоходные в преимуществах:</h3>

                    <ul>
                        <li>Компактность размеров, позволяющая технике беспроблемно проезжать в помещение, работать в условиях суженных пространств.</li>
                        <li>Маневренность решения, минимальный радиус при совершении разворота.</li>
                        <li>Высокая грузоподъемность, наличие ограничителя превышения допустимой массы груза и плавность подъема.</li>
                        <li>Достаточно простое управление, наличие дополнительных возможностей (складывание перил, увеличение площади люльки и иное).</li>
                    </ul>
                </div>
            </section>
            <section>
                <h3 class="center">Таблица расценок на телескопические погрузчики:</h3>
                <p class="center pb-4">В случае длительных заявок возможна договорная цена и скидки</p>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="pb-4">Техника</th>
                            <th>Высота<br> автовышки, м.</th>
                            <th>Цена за смену<br> (8 часов)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $flag = 0 ?>
                        <?php foreach ($lifts as $lift) { $flag++?>
                            <tr>
                                <?php if($flag == 1){ ?>
                                    <td rowspan="<?php echo count($lifts) ?>" class="pt-4">
                                        <img src="/img/lift1.png" alt="Автовышки">
                                    </td>
                                <?php } ?>
                                <td><?php echo $lift->working_height ?></td>
                                <td><?php echo $lift->amount ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>
