<?php
\frontend\assets\ContactAsset::register($this);

/* @var $this yii\web\View */
$mts =  \common\models\Settings::find()->where(['label'=>'Мтс'])->one()->text;
$velcom = \common\models\Settings::find()->where(['label'=>'Velcom'])->one()->text;
$life = \common\models\Settings::find()->where(['label'=>'Life'])->one()->text;
$fax = \common\models\Settings::find()->where(['label'=>'Факс'])->one()->text;
$email = \common\models\Settings::find()->where(['label'=>'Email'])->one()->text;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">Контакты</span>
    </nav>
</div>
<div class="container">
    <h3 class="pt-3">Наши контакты</h3>
    <div class="line"></div>
    <div class="row">
        <div class="col-md-7 col-lg-4 pr-0">
            <div class="contact-text">
                <p class="text-bold">ООО «ЯнТранс»</p>
                <?= \common\models\Settings::find()->where(['label'=>'Контакты адрес'])->one()->text; ?>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="company-info pb-3">
                <p class="text-bold">Телефоны</p>
                <p class="velcom"><a href="tel:<?= $velcom?>"><?= $velcom?></a></p>
                <p class="mts"><a href="tel:<?= $mts ?>"><?= $mts?></a></p>
                <p class="life"><a href="tel:<?= $life?>"><?= $life?></a></p>
                <p class="fax"><a href="#"><?= $fax?><span> (факс)</span></a></p>
                <p class="text-bold pt-2">E-mail</p>
                <p class="email"><a href="mailto:<?= $email?>"><?= $email?></a></p>
            </div>
        </div>
        <div class="col-12 col-lg-4  pl-1">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Готово!</h4>
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>
            <p class="color-silver">Cвяжитесь с нами, если у вас <br>возникли вопросы, мы обязательно вам ответим!</p>
            <?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'contact-form',
                ]
            ]) ?>
            <div class="form-group">
                <?= $form->field($model, 'email')->input('email')->label('Ваш E-mail'); ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'message')->textarea()->label('Сообщениe') ?>
            </div>
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary mt-1']) ?>
        </div>
        <?php ActiveForm::end() ?>

    </div>
</div>
<div class="map">
    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A833cb446da39304d780bd72aacd89bb76c6f4b33b082b7604b7f26a135f3e1fd&amp;source=constructor"
            width="100%" height="328" frameborder="0"></iframe>
</div>
