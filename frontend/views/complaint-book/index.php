<?php

\frontend\assets\ComplaintBookAsset::register($this);

/* @var $this yii\web\View */
$settings = \common\models\Settings::find()->one();
$phones = json_decode($settings->numbers, true);
$this->title = 'Аренда спецтехники в Минске';
?>
<div class="container">
    <nav class="breadcrumb">
        <a class="breadcrumb-item" href="/">Главная</a>
        <span class="breadcrumb-item active">Книга жалоб</span>
    </nav>
</div>
<div class="container complaint">
    <h3 class="pt-3">Жалобная книга</h3>
    <div class="line mb-3"></div>
    <p class="mb-4">Для улучшения качества обслуживания, просим вас заполнить форму.</p>
    <div class="row">
        <div class="wrapper-content">
            <div class="wrapper-form">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                <?php endif; ?>
                <?php
                $form = \yii\widgets\ActiveForm::begin([
                    'options' => [
                        'class' => 'complaint-form',
                    ]
                ]) ?>
                <div class="form-group">
                    <?= $form->field($model, 'nickname')->label('Имя*'); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'email')->input('email')->label('E-mail*'); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'message')->textarea()->label('Текст сообщения*') ?>
                </div>
                <?= \yii\helpers\Html::submitButton('Отправить', ['class' => 'btn btn-primary mt-1 mr-2']) ?>

            <?php \yii\widgets\ActiveForm::end() ?>
            </div>
        </div>

        <div class="wraper-phone">
            <p class="mb-1">Или можете позвонить по телефону:</p>
            <a class="tel" href="tel:<?= $phones['life']?>"><?= $phones['life']?></a>
        </div>
    </div>
</div>
