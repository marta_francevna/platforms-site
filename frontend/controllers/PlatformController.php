<?php

namespace frontend\controllers;

use common\models\Platforms;
use yii\web\Controller;

/**
 * Platform controller
 */
class PlatformController extends Controller
{

    public function actionIndex($id)
    {
        $platform = Platforms::findOne($id);
        return $this->render('index', [
            'platform' => $platform
        ]);

    }


}
