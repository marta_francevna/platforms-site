<?php

namespace frontend\controllers;

use common\forms\OrderForm;
use common\models\Orders;
use common\models\Platforms;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Order controller
 */
class OrderController extends Controller
{

    public function actionIndex($id)
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'Заказать автовышку в Минске'
        ]);
        $model = new OrderForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $order = new Orders();
            $order->attributes = \Yii::$app->request->post('OrderForm');
            if ($order->validate()) {
                if ($order->image = UploadedFile::getInstance($model, 'image')) {
                    $order->image->saveAs('uploads/' . $order->image->baseName . '.' . $order->image->extension, false);
                }
                $order->save(false);
                \Yii::$app->getSession()->setFlash('success', 'Заказ отправлен');

                \Yii::$app->mailer->getView()->params['title'] = 'Поступил новый заказ!';
                $params = [
                    'name' => $order->nickname,
                    'phone' => $order->phone,
                    'email' => $order->email,
                    'message' => $order->message,
                ];
                if ($id){
                    $params['platform'] = Platforms::findOne($id)->name;
                }
                \Yii::$app->mailer->compose([
                    'html' => 'views/order-html',
                    'text' => 'views/order-text',
                ], $params)->setSubject('Новый заказ с Vishki.by')
                    ->send();

                \Yii::$app->mailer->getView()->params['title'] = 'Ваш заказ принят';

                \Yii::$app->mailer->compose([
                    'html' => 'views/order-html',
                    'text' => 'views/order-text',
                ], $params)
                    ->setTo($order->email)
                    ->setSubject('Ваш заказ принят')
                    ->send();


                return $this->render('index', ['model' => $model, ]);
            }

        } else {
	        if ($id){
		        $model->platform_id = $id;
	        }
            return $this->render('index', ['model' => $model]);
        }
    }


}
