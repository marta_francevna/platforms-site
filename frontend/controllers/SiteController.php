<?php
namespace frontend\controllers;

use common\forms\LoginForm;
use common\models\Documents;
use common\models\Platforms;
use common\models\Settings;
use yii\web\Controller;
use yii\filters\AccessControl;
use Yii;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'ООО «ЯнТранс» предоставляет услуги аренды автовышки в Минске и Беларуси, а также сдает внаем телескопические погрузчики и самоходные подъемники. Аренда спецтехники в Минске: от 12 до 40 метров!'
        ]);
        return $this->render('index');
    }

    public function actionAbout()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'О компании ООО "ЯнТранс" и услугах по аренде автовышек'
        ]);
        return $this->render('about');
    }

    public function actionDocuments()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'Заявка на аренду автоподъемника и другой техники'
        ]);
        $doc = Documents::find()->all();
        return $this->render('documents',['documents'=> $doc]);
    }

    public function actionRentalInformation($tab)
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'Арендовать автовышку, телескопический погрузчик или самоходный подъемник в Минске по привлекательной цене'
        ]);
        $platform = Platforms::find()->where(['type'=>Platforms::PLATFORMS])->all();
        $loader = Platforms::find()->where(['type'=>Platforms::LOADER])->all();
        $lift = Platforms::find()->where(['type'=>Platforms::LIFT])->all();

        return $this->render('rental-information', [
            'platforms' => $platform,
            'loaders' => $loader,
            'lifts' => $lift,
	        'tab' => $tab
        ]);
    }

    public function actionRepair(){

        return $this->render('repair');
    }

    public function actionContacts(){

        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'Связаться с ООО «ЯнТранс».'
        ]);
        return $this->render('contacts');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = false;
        if (!Yii::$app->user->isGuest) {
            $this->redirect('admin/index');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect('admin');
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }



}
