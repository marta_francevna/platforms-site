<?php

namespace frontend\controllers;

use common\forms\ComplaintForm;
use common\models\Complaint;
use yii\web\Controller;

/**
 * Complaint controller
 */
class ComplaintBookController extends Controller
{

    public function actionIndex()
    {
        $model = new ComplaintForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $complaint = new Complaint();
            $complaint->attributes = \Yii::$app->request->post('ComplaintForm');
            $complaint->save();
            \Yii::$app->getSession()->setFlash('success', 'Сообщение отправлено');

            \Yii::$app->mailer->getView()->params['title'] = 'Поступила новая жалоба';
            $params = [
                'email' => $complaint->email,
                'message' => $complaint->message,
            ];
            \Yii::$app->mailer->compose([
                'html' => 'views/html',
                'text' => 'views/text',
            ], $params)->setSubject('Новая жалоба с Vishki.by')->send();

            return $this->render('index', ['model' => $model]);

        } else {
            return $this->render('index', ['model' => $model]);
        }
    }


}
