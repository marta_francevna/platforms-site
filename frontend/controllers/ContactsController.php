<?php
namespace frontend\controllers;

use common\forms\FeedbackForm;
use common\models\Feedback;
use yii\web\Controller;

/**
 * IContacts controller
 */
class ContactsController extends Controller
{

    public function actionIndex()
    {
    	$model = new FeedbackForm();
	    if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
	        $feedback = new Feedback();
	        $feedback->attributes = \Yii::$app->request->post('FeedbackForm');
	        $feedback->save();
            \Yii::$app->getSession()->setFlash('success', 'Сообщение отправлено');

            \Yii::$app->mailer->getView()->params['title'] = 'Новое сообщение обратной связи';
            $params = [
                'email' => $feedback->email,
                'message' => $feedback->message,
            ];
            \Yii::$app->mailer->compose([
                'html' => 'views/html',
                'text' => 'views/text',
            ], $params)->setSubject('Обратная связь с Vishki.by')
                ->send();

		    return $this->render('index', ['model' => $model]);
	    } else {

		    return $this->render('index', ['model' => $model]);
	    }
    }


}
