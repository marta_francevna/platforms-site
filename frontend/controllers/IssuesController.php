<?php
namespace frontend\controllers;

use common\forms\IssuesForm;
use common\models\Issues;
use yii\web\Controller;

/**
 * Issues controller
 */
class IssuesController extends Controller
{

    public function actionIndex()
    {
        $issues = Issues::find()->with('question')->where(['question_id'=>null])->all();
       $model = new IssuesForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $issue = new Issues();
            $issue->attributes = \Yii::$app->request->post('IssuesForm');
            $issue->save();
            \Yii::$app->getSession()->setFlash('success', 'Сообщение отправлено');

            \Yii::$app->mailer->getView()->params['title'] = 'Новый вопрос';
            $params = [
                'email' => $issue->email,
                'message' => $issue->message,
            ];
            \Yii::$app->mailer->compose([
                'html' => 'views/html',
                'text' => 'views/text',
            ], $params)->setSubject('Новый вопрос с Vishki.by')
                ->send();

            return $this->render('index', ['model' => $model, 'issues' => $issues]);
        } else {

            return $this->render('index', ['model' => $model, 'issues' => $issues]);
        }
    }


}
