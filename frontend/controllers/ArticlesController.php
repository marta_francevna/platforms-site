<?php
namespace frontend\controllers;

use common\models\Articles;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Articles controller
 */
class ArticlesController extends Controller
{

    public function actionIndex()
    {
        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => 'Статьи о преимуществах и нюансах использования автовышек'
        ]);
        $articles = Articles::find();
        $dataProvider = new ActiveDataProvider( [ 'query' => $articles ] );
        $dataProvider->pagination = false;

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }


    public function actionView($slug)
    {
	    $model = Articles::findOne(['slug' => $slug]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $model->title
        ]);
        return $this->render('view', [
            'model' => $model
        ]);
    }

}
