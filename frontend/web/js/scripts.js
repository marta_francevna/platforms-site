

$('.spoiler_links').click(function()
{
    var $other_spoilers = $('a.spoiler_links').not(this);

    $other_spoilers.parent().siblings('.answer').hide();
    $other_spoilers.find('.icon-arrow-up').removeClass("icon-arrow-up").addClass('icon-arrow-down');

    $(this).parent().siblings('.answer').toggle();

    var icon = $(this).children();
    if (icon.hasClass( "icon-arrow-up" )){
        icon.removeClass("icon-arrow-up").addClass('icon-arrow-down');
    }else{
        icon.removeClass("icon-arrow-down").addClass('icon-arrow-up');
    }

     return false;
});


$(document).ready( function() {
    $(".file-upload input[type=file]").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $("#filename").val(filename);
    });
});

