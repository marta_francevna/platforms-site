<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class PlatformAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/platform.css',
        'css/magnific.css',
    ];
    public $js = [
        'js/magnific.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'mimicreative\assets\SimpleLineIconsAsset'
    ];
}
