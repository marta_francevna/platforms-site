<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class DocumentsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/documents.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'mimicreative\assets\SimpleLineIconsAsset'
    ];
}
