<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180620_205215_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'document'=> $this->string(),
            'numbers'=> $this->string(),
            'email'=> $this->string(),
        ]);

        $phones = [
            'life' => '+375 (44) 544-55-55',
            'mts' => '+375 (44) 544-55-55',
            'welcome' => '+375 (44) 55-55-55',
            'fax' => '+8 (017) 223-38-38'
        ];
        $this->insert('settings', [
            'numbers' => json_encode($phones),
            'email' => '80445445555@mail.ru',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
