<?php

use yii\db\Migration;

/**
 * Class m190107_175545_update_platforms_table
 */
class m190107_175545_update_platforms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('platforms','description',  $this->text());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('platforms','description');
    }

}
