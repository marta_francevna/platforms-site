<?php

use yii\db\Migration;

/**
 * Class m190107_170704_update_platforms_table
 */
class m190107_170704_update_platforms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('platforms','eparture',  $this->string());
        $this->addColumn('platforms','rise_time',  $this->string());
        $this->addColumn('platforms','transport_speed',  $this->string());
        $this->addColumn('platforms','angle_of_rotation',  $this->string());
        $this->addColumn('platforms','operating_weight',  $this->string());
        $this->addColumn('platforms','transport_height',  $this->string());
        $this->addColumn('platforms','transport_length',  $this->string());
        $this->addColumn('platforms','transport_width',  $this->string());
        $this->addColumn('platforms','chassis',  $this->string());
        $this->addColumn('platforms','power',  $this->string());
        $this->addColumn('platforms','scheme',  $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('platforms','eparture');
        $this->dropColumn('platforms','rise_time');
        $this->dropColumn('platforms','transport_speed');
        $this->dropColumn('platforms','angle_of_rotation');
        $this->dropColumn('platforms','operating_weight');
        $this->dropColumn('platforms','transport_height');
        $this->dropColumn('platforms','transport_length');
        $this->dropColumn('platforms','transport_width');
        $this->dropColumn('platforms','chassis');
        $this->dropColumn('platforms','power');
        $this->dropColumn('platforms','scheme');
    }



}
