<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m180614_151821_create_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
	        'title' => $this->string()->notNull(),
	        'description' => $this->string(),
	        'slug' => $this->string()->notNull()->unique(),
	        'content' => $this->text()->notNull()
	        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles');
    }
}
