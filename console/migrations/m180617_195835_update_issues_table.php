<?php

use yii\db\Migration;

/**
 * Class m180617_195835_update_issues_table
 */
class m180617_195835_update_issues_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('issues','question_id',  $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('issues', 'question_id');

    }

}
