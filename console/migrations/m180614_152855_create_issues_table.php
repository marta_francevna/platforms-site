<?php

use yii\db\Migration;

/**
 * Handles the creation of table `issues`.
 */
class m180614_152855_create_issues_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('issues', [
            'id' => $this->primaryKey(),
	        'nickname' => $this->string(50)->notNull(),
	        'email' => $this->string(50)->notNull(),
	        'message' => $this->string(225)->notNull(),
	        'created_at' => $this->timestamp()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('issues');
    }
}
