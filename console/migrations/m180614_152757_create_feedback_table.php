<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m180614_152757_create_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
	        'email' => $this->string(50)->notNull(),
	        'message' => $this->string(225)->notNull(),
	        'created_at' => $this->timestamp()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback');
    }
}
