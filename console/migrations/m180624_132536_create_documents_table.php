<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documents`.
 */
class m180624_132536_create_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('documents', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'file'=> $this->string()->notNull()
        ]);

        $this->insert('documents', [
            'name' => 'Заявка на аренду автоподъемника',
            'file' => 'Заявка.docx',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('documents');
    }
}
