<?php

use yii\db\Migration;

/**
 * Class m180620_145204_add_column_to_order_table
 */
class m180620_145204_add_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('orders','platform_id',  $this->integer());
	    $this->createIndex(
		    'idx-orders-platforms_id',
		    'orders',
		    'platform_id'
	    );
	    $this->addForeignKey(
		    'fk-orders-platforms_id',
		    'orders',
		    'platform_id',
		    'platforms',
		    'id',
		    'CASCADE'
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropForeignKey(
		    'fk-orders-platforms_id',
		    'orders'
	    );

	    // drops index for column `author_id`
	    $this->dropIndex(
		    'idx-orders-platforms_id',
		    'orders'
	    );

	    $this->dropColumn('orders', 'platform_id');
    }

}
