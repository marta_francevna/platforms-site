<?php

use yii\db\Migration;

/**
 * Class m180619_213326_add_columns_to_articles_table
 */
class m180619_213326_add_columns_to_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('articles','keywords',  $this->string());
        $this->addColumn('articles','descriptions',  $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('articles', 'keywords');
        $this->dropColumn('articles', 'descriptions');
    }


}
