<?php

use yii\db\Migration;

/**
 * Class m180624_150658_add_column_to_documents_table
 */
class m180624_150658_add_column_to_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('documents','size',  $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('documents', 'size');
    }


}
