<?php

use yii\db\Migration;

/**
 * Handles the creation of table `platforms`.
 */
class m180617_222848_create_platforms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('platforms', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->tinyInteger()->notNull(),
            'working_height' => $this->string(),
            'working_capacity' => $this->string(),
            'arrow' => $this->string(),
            'seats' => $this->integer(),
            'image' => $this->string(),
            'amount' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('platforms');
    }
}
