<?php

use yii\db\Migration;

/**
 * Handles the creation of table `complaint`.
 */
class m180614_152447_create_complaint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('complaint', [
            'id' => $this->primaryKey(),
	        'nickname' => $this->string(50)->notNull(),
	        'email' => $this->string(50)->notNull(),
	        'message' => $this->string(225)->notNull(),
	        'created_at' => $this->timestamp()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('complaint');
    }
}
