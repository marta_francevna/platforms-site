<?php

use yii\db\Migration;

/**
 * Class m190113_110100_update_settings_table
 */
class m190113_110100_update_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('settings', 'numbers');
        $this->dropColumn('settings', 'email');
        $this->addColumn('settings','label',  $this->string());
        $this->addColumn('settings','text',  $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('settings', 'label');
        $this->dropColumn('settings', 'text');
        $this->addColumn('settings','numbers',  $this->string());
        $this->addColumn('settings','email',  $this->string());
    }

}
