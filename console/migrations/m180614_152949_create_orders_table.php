<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180614_152949_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
	        'nickname' => $this->string(50)->notNull(),
	        'phone' => $this->string(50)->notNull(),
	        'email' => $this->string(50)->notNull(),
	        'image' => $this->string(255),
	        'message' => $this->string(500)->notNull(),
	        'created_at' => $this->timestamp()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }
}
