<?php

use yii\db\Migration;

/**
 * Class m180624_132846_delete_document_column_from_settings_table
 */
class m180624_132846_delete_document_column_from_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('settings', 'document');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('settings', 'document', $this->string());
    }

}
