<?php
namespace common\forms;

class ComplaintForm extends \yii\base\Model
{
    public $nickname;
	public $email;
	public $message;

	public function rules()
	{
		return [
            [['nickname','email', 'message'], 'required'],
            [['nickname', 'email'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 255],
			['email', 'email'],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nickname' => 'Имя',
            'email' => 'E-mail',
            'message' => 'Сообщение',
        ];
    }
}