<?php
namespace common\forms;

class OrderForm extends \yii\base\Model
{
    public $nickname;
    public $phone;
	public $email;
	public $image;
	public $message;
	public $platform_id;

	public function rules()
	{
		return [
            [['nickname', 'phone', 'email', 'message'], 'required'],
            [['nickname', 'phone', 'email'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 500],
			['email', 'email'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'image' => 'Изображение',
            'nickname' => 'Имя',
            'email' => 'E-mail',
            'message' => 'Сообщение',
        ];
    }
}