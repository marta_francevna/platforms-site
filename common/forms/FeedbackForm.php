<?php
namespace common\forms;

class FeedbackForm extends \yii\base\Model
{

	public $email;
	public $message;

	public function rules()
	{
		return [
            [['email', 'message'], 'required'],
            [['email'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 255],
			['email', 'email'],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'message' => 'Сообщение',
        ];
    }
}