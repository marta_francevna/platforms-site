<?php
namespace common\forms;

class SettingsForm extends \yii\base\Model
{
    public $life;
	public $mts;
	public $welcome;
	public $fax;
	public $email;

	public function rules()
	{
		return [
            [['life','mts', 'welcome', 'fax', 'email'], 'required'],
            [['life', 'mts', 'welcome','fax','email'], 'string', 'max' => 25],
			['email', 'email'],
		];
	}
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'life' => 'Life',
            'mts' => 'МТС',
            'welcome' => 'Welcome',
            'fax' => 'Факс',
            'email' => 'E-mail',
        ];
    }
}