<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "issues".
 *
 * @property int $id
 * @property string $nickname
 * @property string $email
 * @property string $message
 * @property string $created_at
 * @property int $question_id
 */
class Issues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'issues';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nickname', 'email', 'message'], 'required'],
            [['created_at'], 'safe'],
            [['question_id'], 'integer'],
            [['nickname', 'email'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 225],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nickname' => 'Имя',
            'email' => 'Email',
            'message' => 'Сообщение',
            'created_at' => 'Created At',
            'question_id' => 'Question ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Issues::className(), ['question_id' => 'id']);
    }
}
