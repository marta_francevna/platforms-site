<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "platforms".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 * @property string $working_height
 * @property string $working_capacity
 * @property string $arrow
 * @property int $seats
 * @property string $image
 * @property string $amount
 * @property string $eparture
 * @property string $rise_time
 * @property string $transport_speed
 * @property string $angle_of_rotation
 * @property string $operating_weight
 * @property string $transport_height
 * @property string $transport_length
 * @property string $transport_width
 * @property string $chassis
 * @property string $power
 * @property string $scheme
 * @property string $description
 *
 * @property Orders[] $orders
 */
class Platforms extends \yii\db\ActiveRecord
{
    const PLATFORMS = 1;
    const LOADER = 2;
    const LIFT = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platforms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type', 'seats'], 'integer'],
            [['description'], 'string'],
            [['name', 'working_height', 'working_capacity', 'arrow', 'image', 'amount', 'eparture', 'rise_time', 'transport_speed', 'angle_of_rotation', 'operating_weight', 'transport_height', 'transport_length', 'transport_width', 'chassis', 'power', 'scheme'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'type' => 'Тип',
            'working_height' => 'Рабочая высота',
            'working_capacity' => 'Грузоподъемность',
            'arrow' => 'Тип стрелы',
            'seats' => 'Кол-во мест',
            'image' => 'Изрбражение',
            'amount' => 'Стоимость',
            'eparture' => 'Вылет',
            'rise_time' => 'Время подъёма',
            'transport_speed' => 'Транспортная скорость',
            'angle_of_rotation' => 'Угол поворота',
            'operating_weight' => 'Эксплуатационная масса',
            'transport_height' => 'Транспортная высота',
            'transport_length' => 'Транспортная длина',
            'transport_width' => 'Транспортная ширина',
            'chassis' => 'Шасси',
            'power' => 'Мощность',
            'scheme' => 'Схема',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['platform_id' => 'id']);
    }
}
