<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $content
 * @property string $keywords
 * @property string $descriptions
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articles';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content'], 'string'],
            [['title', 'description', 'slug'], 'string', 'max' => 225],
            [['keywords', 'descriptions'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'slug' => 'Slug',
            'content' => 'Контент',
            'keywords' => 'Keywords',
            'descriptions' => 'Descriptions',
        ];
    }
}
