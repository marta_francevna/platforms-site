<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $nickname
 * @property string $phone
 * @property string $email
 * @property string $image
 * @property string $message
 * @property integer $platform_id
 * @property string $created_at
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nickname', 'phone', 'email', 'message'], 'required'],
            [['created_at'], 'safe'],
	        [['platform_id'], 'integer'],
            [['nickname', 'phone', 'email'], 'string', 'max' => 50],
            [['message'], 'string', 'max' => 500],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nickname' => 'Nickname',
            'phone' => 'Phone',
            'email' => 'Email',
            'image' => 'Image',
            'message' => 'Message',
            'created_at' => 'Created At',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPlatform()
	{
		return $this->hasOne(Platforms::className(), ['id' => 'platform_id']);
	}
}
