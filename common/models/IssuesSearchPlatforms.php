<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Platforms;

/**
 * IssuesSearchPlatforms represents the model behind the search form of `common\models\Platforms`.
 */
class IssuesSearchPlatforms extends Platforms
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'seats'], 'integer'],
            [['name', 'working_height', 'arrow', 'image', 'amount'], 'safe'],
            [['working_capacity'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Platforms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'working_capacity' => $this->working_capacity,
            'seats' => $this->seats,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'working_height', $this->working_height])
            ->andFilterWhere(['like', 'arrow', $this->arrow])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'amount', $this->amount]);

        return $dataProvider;
    }
}
